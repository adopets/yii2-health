<?php
namespace Adopets\health;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'hardtyz\health\controllers';
    public $helperNamespace = 'hardtyz\health\helpers';
    public $modelsNamespace = 'hardtyz\health\models';
    public $defaultRoute = 'default';
    public $check = [];
}
