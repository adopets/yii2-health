<?php

namespace Adopets\health\models;

use yii\base\Model;
use hardtyz\health\helpers\TcpHelper;
use hardtyz\health\helpers\HttpHelper;
use hardtyz\health\helpers\DbHelper;

class Item extends Model
{
    public $name;
    public $type;
    public $port;
    public $host;
    public $db;
    private $status;

    public function rules()
    {
        return [
        ];
    }


    public function attributeLabels()
    {
        return [
            'type'          => 'Check type',
            'host' => 'Hostname or IP adress',
            'port'  => 'Port',

        ];
    }

    public function getStatus() {
        switch ($this->type){
            case 'tcp':
                return TcpHelper::check($this->host, $this->port);
                break;
            case 'http':
                return HttpHelper::check($this->host);
                break;
            case 'db':
                return DbHelper::check($this->db);
                break;
        }
    }
}
