<?php


namespace Adopets\health\helpers;
use yii;

class DbHelper
{
    static function check($db) {
        try {
            Yii::$app->$db->open();
            Yii::$app->$db->close();
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}
